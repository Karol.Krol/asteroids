import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { combineReducers, createStore, applyMiddleware } from "redux";
// redux-logger log every state change
import logger from "redux-logger";
import appReducer, { AppContainer } from "./app";
import spaceShipReducer from "./spaceship";

const middleware = applyMiddleware(logger);
const rootReducer = combineReducers({
  app: appReducer,
  spaceShip: spaceShipReducer
});
const store = createStore(rootReducer, middleware);

ReactDOM.render(
  <Provider store={store}>
    <AppContainer />
  </Provider>,
  document.getElementById("app")
);
