import { connect } from "react-redux";
import SpaceShip from "./SpaceShip";
import { Map } from "immutable";

const initializeStateWithInitProps = (state, props) => {
  state.spaceShip = Map({
    position: {
      x: props.position.x,
      y: props.position.y
    },
    rotation: props.rotation,
    speed: props.speed,
    radius: props.radius
  });
};

const mapStateToProps = (state, props) => {
  if (state.spaceShip.size === 0) {
    initializeStateWithInitProps(state, props);
  }
  return {
    position: state.spaceShip.get("position"),
    rotation: state.spaceShip.get("rotation"),
    radius: state.spaceShip.get("radius"),
    speed: state.spaceShip.get("speed")
  };
};

const mapDispatchToProps = dispatch => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SpaceShip);
