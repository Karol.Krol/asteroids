import { default as spaceShipReducer } from "./SpaceShipReducer";
export { default as SpaceShipContainer } from "./SpaceShipContainer";
export { default as spaceShipActions } from "./SpaceShipActions";
export default spaceShipReducer;
