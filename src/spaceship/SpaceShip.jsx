import React from "react";
import { Line } from "react-konva";

export default function SpaceShip({ position, rotation, radius, speed }) {
  return (
    <Line
      x={position.x}
      y={position.y}
      points={[
        -(radius * 2),
        -radius,
        radius * 2,
        0,
        -(radius * 2),
        radius,
        -radius,
        0
      ]}
      strokeWidth={3}
      stroke="#fff000"
      closed="true"
      rotation={rotation}
    />
  );
}
