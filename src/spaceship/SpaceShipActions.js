import { createActions } from "reduxsauce";

const { Types, Creators } = createActions(
  {
    rotateShipRight: ['rotate'],
    rotateShipLeft: ['rotate'],
    accelerateShip: ['increaseSpeed'],
    slowDownShip: ['decreaseSpeed'],
    update: []
  },
  {}
);

export { Types };
export default Creators;
