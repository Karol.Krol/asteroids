import { createReducer } from "reduxsauce";
import { Map } from "immutable";
import { recalculatePositionWhileBorderCrossed } from "../helpers/gameObjectHelper";
import { Types } from "./SpaceShipActions";

const INITIAL_STATE = Map({});

const rotateRight = (state = INITIAL_STATE, action) => {
  return state.set("rotation", state.get("rotation") + action.rotate);
};

const rotateLeft = (state = INITIAL_STATE, action) => {
  return state.set("rotation", state.get("rotation") - action.rotate);
};

const accelerate = (state = INITIAL_STATE, action) => {
  return state.set("speed", state.get("speed") + action.increaseSpeed);
};

const slowDown = (state = INITIAL_STATE, action) => {
  const newSpeed = state.get("speed") - action.decreaseSpeed;
  return newSpeed < 0 ? state : state.set("speed", newSpeed);
};

const update = (state = INITIAL_STATE, action) => {
  const speed = state.get("speed");
  if (speed > 0) {
    const rotation = state.get("rotation");
    const delX = Math.cos((rotation * Math.PI) / 180) * speed;
    const delY = Math.sin((rotation * Math.PI) / 180) * speed;
    const currentPosition = state.get("position");
    return state.set(
      "position",
      recalculatePositionWhileBorderCrossed({
        x: currentPosition.x + delX,
        y: currentPosition.y + delY
      })
    );
  }
  return state;
};

const HANDLERS = {
  [Types.ROTATE_SHIP_RIGHT]: rotateRight,
  [Types.ROTATE_SHIP_LEFT]: rotateLeft,
  [Types.ACCELERATE_SHIP]: accelerate,
  [Types.SLOW_DOWN_SHIP]: slowDown,
  [Types.UPDATE]: update
};

export default createReducer(INITIAL_STATE, HANDLERS);
