export default {
  CANVAS_VIRTUAL_WIDTH: 800,
  ASPECT: 1,
  FRAME_RATE_MS: 20
};
