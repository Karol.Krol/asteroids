import settings from "./gameSettings"
export const recalculatePositionWhileBorderCrossed = (position) => {
    const maxWidth = settings.CANVAS_VIRTUAL_WIDTH;
    const maxHeight = settings.CANVAS_VIRTUAL_WIDTH * settings.ASPECT;
    let posX = position.x;
    let posY = position.y;
    if (posX > maxWidth) {
      posX = posX % maxWidth;
    } else if (posX < 0) {
      posX = posX + maxWidth;
    }
    if (posY > (maxHeight)){
      posY = posY % (maxHeight);
    } else if (posY < 0) {
      posY = posY + (maxHeight);
    }
    return { x: posX, y: posY };
  };