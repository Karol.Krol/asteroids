import React from "react";
import { Stage, Rect, Layer } from "react-konva";
import settings from "../helpers/gameSettings";
import { SpaceShipContainer } from "../spaceship";

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.handleResize = this.handleResize.bind(this);
    this.keyPress = this.keyPress.bind(this);
  }

  componentDidMount() {
    window.addEventListener("resize", this.handleResize);
    window.addEventListener("keydown", this.keyPress);
    const { frameRateMs } = this.props;
    this.timerId = setInterval(() => this.update(), frameRateMs);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleResize);
    window.removeEventListener("keydown", this.keyPress);
    clearInterval(this.timerId);
  }

  handleResize(event) {
    console.log(event);
    const { onWindowResize } = this.props;
    onWindowResize();
  }

  keyPress(event) {
    const { onKeyPress } = this.props;
    onKeyPress(event.code);
  }

  update() {
    const { onUpdate, width, height } = this.props;
    onUpdate(width, height);
  }

  render() {
    const { scale, size } = this.props;
    return (
      <Stage
        width={size}
        height={size * settings.ASPECT}
        scaleX={scale}
        scaleY={scale}
      >
        <Layer>
          <Rect
            width={settings.CANVAS_VIRTUAL_WIDTH}
            height={settings.CANVAS_VIRTUAL_WIDTH * settings.ASPECT}
            fill="#000000"
          />
          <SpaceShipContainer
            position={{
              x: Math.round(settings.CANVAS_VIRTUAL_WIDTH / 2),
              y: Math.round(
                (settings.CANVAS_VIRTUAL_WIDTH * settings.ASPECT) / 2
              )
            }}
            rotation={0}
            radius={settings.CANVAS_VIRTUAL_WIDTH / 100}
            speed={0}
          />
        </Layer>
      </Stage>
    );
  }
}
