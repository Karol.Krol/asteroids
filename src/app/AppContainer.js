import { connect } from "react-redux";
import App from "./App";
import Creators from "./AppActions";
import { spaceShipActions } from "../spaceship";

const mapStateToProps = state => {
  return {
    scale: state.app.get("scale"),
    frameRateMs: state.app.get("frameRateMs"),
    size: state.app.get("size")
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onWindowResize: () =>
      dispatch(Creators.windowResize()),
    onKeyPress: keyCode => {
      switch (keyCode) {
        case "ArrowRight":
          dispatch(spaceShipActions.rotateShipRight(4));
          break;
        case "ArrowLeft":
          dispatch(spaceShipActions.rotateShipLeft(4));
          break;
        case "ArrowUp":
          dispatch(spaceShipActions.accelerateShip(1));
          break;
        case "ArrowDown":
          dispatch(spaceShipActions.slowDownShip(1));
          break;
      }
    },
    onUpdate: () => {
      dispatch(spaceShipActions.update());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
