import { default as appReducer } from "./AppReducer";
export { default as AppContainer } from "./AppContainer";
export default appReducer;