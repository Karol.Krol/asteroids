import { createActions } from "reduxsauce";

const { Types, Creators } = createActions(
  {
    windowResize: [],
    keyPress: ['keyCode']
  },
  {}
);

export { Types };
export default Creators;