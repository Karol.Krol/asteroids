import { createReducer } from "reduxsauce";
import { Map } from "immutable";
import { Types } from "./AppActions";
import settings from "../helpers/gameSettings";

const getCurrentLength = () =>
  Math.min(window.innerWidth, window.innerHeight) - 15;

const INITIAL_STATE = Map({
  scale: getCurrentLength() / settings.CANVAS_VIRTUAL_WIDTH,
  size: getCurrentLength(),
  frameRateMs: settings.FRAME_RATE_MS
});

const windowResize = (state, action) => {
  const length = getCurrentLength();
  return state
    .set("scale", length / settings.CANVAS_VIRTUAL_WIDTH)
    .set("size", getCurrentLength());
};

const HANDLERS = {
  [Types.WINDOW_RESIZE]: windowResize
};

export default createReducer(INITIAL_STATE, HANDLERS);
